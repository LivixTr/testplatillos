import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BienvenidaComponent } from './components/bienvenida/bienvenida.component';
import { IngredientesPopularesComponent } from './components/ingredientes-populares/ingredientes-populares.component';
import { DetallePlatilloComponent } from './components/detalle-platillo/detalle-platillo.component';
import { LoginComponent } from './components/login/login.component';
import { PerfilUsuarioComponent } from './components/perfil-usuario/perfil-usuario.component';
import { PlatillosComponent } from './components/platillos/platillos.component';
import { PlatillosIngredienteComponent } from './components/platillos-ingrediente/platillos-ingrediente.component';


const appRoutes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'Bienvenida', component: BienvenidaComponent },
    { path: 'PerfilUsuario', component: PerfilUsuarioComponent },
    { path: 'Platillos', component: PlatillosComponent },
    { path: 'IngredientesPopulares', component: IngredientesPopularesComponent },
    { path: 'DetallePlatillo', component: DetallePlatilloComponent },
    { path: 'PlatillosIngrediente', component: PlatillosIngredienteComponent },
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);