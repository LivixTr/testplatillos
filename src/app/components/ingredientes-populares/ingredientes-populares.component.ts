import { Component, OnInit, ViewChild } from '@angular/core';
import { IngredientesService } from 'src/app/services/Ingredientes/ingredientes.service';
import { MatTableDataSource } from '@angular/material/table';
import { IngredientesPopulares } from 'src/app/models/ingredientes'; 
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingredientes-populares',
  templateUrl: './ingredientes-populares.component.html',
  styleUrls: ['./ingredientes-populares.component.css']
})
export class IngredientesPopularesComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nombre'];
  dataSource!: MatTableDataSource<IngredientesPopulares>;
  tamanioTabla = 0;
  @ViewChild('paginator1', {static: true}) paginator1!: MatPaginator;

  constructor( private ing: IngredientesService, private router: Router) { }

  ngOnInit(): void {
    this.obtenerIngrediente();
  }
  obtenerIngrediente(): void {
    this.ing.obtenerIngredientesPopulares().subscribe((res) => {
      const ingredientes: Array<IngredientesPopulares> = res.meals;
      this.dataSource = new MatTableDataSource(ingredientes);
      this.tamanioTabla = this.dataSource.data.length;
      this.dataSource.paginator = this.paginator1;
      //console.log(res);
    });
  }
  
  verPlatillos(nombre: string): void {
    this.router.navigate(['/PlatillosIngrediente']);
  }

}
