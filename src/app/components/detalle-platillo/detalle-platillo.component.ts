import { Component, OnInit } from '@angular/core';
import { PlatillosService } from 'src/app/services/Platillos/platillos.service'; 
import { DetalleAlimento } from 'src/app/models/alimentos'; 

@Component({
  selector: 'app-detalle-platillo',
  templateUrl: './detalle-platillo.component.html',
  styleUrls: ['./detalle-platillo.component.css']
})
export class DetallePlatilloComponent implements OnInit {

  srcImagen: string = '';
  titulo: string = '';
  instrucciones = '';
  srcVideo = '';

  constructor(private pla: PlatillosService) { }

  ngOnInit(): void {
  }

  obtenerDetalle(idMeal: string): void {
    this.pla.obtenerPlatilloPorId(idMeal).subscribe((res) => {
      const detalle: Array<DetalleAlimento> = res.meals;
      this.srcImagen = detalle[0].strMealThumb;
      this.titulo = detalle[0].strMeal;
      this.instrucciones = detalle[0].strInstructions;
      this.srcVideo = detalle[0].strYoutube.replace('/watch?', '/embed/').replace('v=', '');
    })
  }

}
