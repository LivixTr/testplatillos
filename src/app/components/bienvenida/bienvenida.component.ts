import { Component, OnInit } from '@angular/core';
import { DetalleAlimento } from 'src/app/models/alimentos'; 
import { PlatillosService } from 'src/app/services/Platillos/platillos.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-bienvenida',
  templateUrl: './bienvenida.component.html',
  styleUrls: ['./bienvenida.component.css']
})
export class BienvenidaComponent implements OnInit {
  srcImage = '';
  titulo = '';
  id = '';

  constructor(
    private pla: PlatillosService
  ) { }

  ngOnInit(): void {
  }

  obtenerPlatillo(): void {
    this.pla.obtenerPlatillosRandom().subscribe((res) => {
      const meal: Array<DetalleAlimento> = res.meals;
      this.srcImage = meal[0].strMealThumb;
      this.titulo = meal[0].strMeal;
      this.id = meal[0].idMeal;
    })
  }


}
