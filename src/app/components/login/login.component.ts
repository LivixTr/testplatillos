import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private acces = {
      user: "user",
      password: "root"
    };
  
  public form: any;
  public user: any;

  constructor(public router: Router) { 
    this.user = new User('Javier', 'Martinez', '', '');
    
  }
  

  ngOnInit(): void {
  }
  

  onSubmit(f: NgForm) {
    if ((this.user.user == this.acces.user) && (this.user.password == this.acces.password)) {
      this.router.navigate(['Bienvenida']);
    }

  }

}
