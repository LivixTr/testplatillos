import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing, appRoutingProviders } from './app.routing';
import { AngularMaterialModule } from './angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BienvenidaComponent } from './components/bienvenida/bienvenida.component';
import { IngredientesPopularesComponent } from './components/ingredientes-populares/ingredientes-populares.component';
import { DetallePlatilloComponent } from './components/detalle-platillo/detalle-platillo.component';
import { LoginComponent } from './components/login/login.component';
import { PerfilUsuarioComponent } from './components/perfil-usuario/perfil-usuario.component';
import { PlatillosComponent } from './components/platillos/platillos.component';
import { PlatillosIngredienteComponent } from './components/platillos-ingrediente/platillos-ingrediente.component';
import { ErrorComponent } from './components/error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    BienvenidaComponent,
    IngredientesPopularesComponent,
    DetallePlatilloComponent,
    LoginComponent,
    PerfilUsuarioComponent,
    PlatillosComponent,
    PlatillosIngredienteComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    AngularMaterialModule,
    FlexLayoutModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
