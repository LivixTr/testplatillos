export class User {
    constructor(
        public name: string,
        public surName: string,
        public user: string,
        public password: string
    ) { }
}